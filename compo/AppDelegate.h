//
//  AppDelegate.h
//  compo
//
//  Created by Krzysztof Zabłocki on 11/04/2014.
//  Copyright kryzys 2014. All rights reserved.
//

#import "cocos2d.h"

@interface compoAppDelegate : NSObject <NSApplicationDelegate>
{
	NSWindow	*window_;
	CCGLView	*glView_;
}

@property (assign) IBOutlet NSWindow	*window;
@property (assign) IBOutlet CCGLView	*glView;

- (IBAction)toggleFullScreen:(id)sender;

@end
