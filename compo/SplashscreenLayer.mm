//
// Created by piotr on 12/04/2014.
//


#import "SplashscreenLayer.h"
#import "CCTexture2D.h"
#import "CCSprite.h"
#import "GameLayer.h"


@implementation SplashscreenLayer
{

}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.mouseEnabled = YES;

        CCSprite* background = [[CCSprite alloc] initWithFile:@"crysis.jpg"];
        background.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
        [self addChild:background];
    }

    return self;
}

- (BOOL) ccMouseDown:(NSEvent *)event
{
    CCScene * newScene = [CCScene node];
    [newScene addChild:[GameLayer node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:0.5 scene:newScene]];

    return YES;
}

@end